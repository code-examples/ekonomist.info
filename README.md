# [ekonomist.info](https://ekonomist.info) source codes

<br/>

### Run ekonomist.info on localhost

    # vi /etc/systemd/system/ekonomist.info.service

Insert code from ekonomist.info.service

    # systemctl enable ekonomist.info.service
    # systemctl start ekonomist.info.service
    # systemctl status ekonomist.info.service

http://localhost:4056
